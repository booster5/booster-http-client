package io.gitlab.booster.http.client.config;

import io.micrometer.common.KeyValue;
import io.micrometer.common.KeyValues;
import org.springframework.web.reactive.function.client.ClientHttpObservationDocumentation;
import org.springframework.web.reactive.function.client.ClientRequestObservationContext;
import org.springframework.web.reactive.function.client.DefaultClientRequestObservationConvention;

/**
 * Adds custom tags for {@link org.springframework.web.reactive.function.client.WebClient}
 */
public class BoosterObservationConvention extends DefaultClientRequestObservationConvention {
    /**
     * URI tags
     */
    public static final String URI_ATTRIBUTE = "custom.webclient.uri";

    /**
     * client name tags
     */
    public static final String CLIENT_NAME_ATTRIBUTE = "custom.client.name";

    /**
     * URI name tag
     */
    public static final String URI = "uri";

    /**
     * Constructor to add tags for HTTP clients.
     */
    public BoosterObservationConvention() {
        super("http.client.requests");
    }

    @Override
    public KeyValues getLowCardinalityKeyValues(ClientRequestObservationContext context) {
        KeyValues keyValues = super.getLowCardinalityKeyValues(context);

        return keyValues.and(this.method(context))
                .and(KeyValue.of("uri", this.httpUrl(context).getValue()))
                .and(this.clientNameKeyValue(context));
    }

    private KeyValue clientNameKeyValue(ClientRequestObservationContext context) {
        return context.getRequest() == null ? KeyValue.of(ClientHttpObservationDocumentation.LowCardinalityKeyNames.CLIENT_NAME, this.clientName(context).getValue()) :
                context.getRequest()
                        .attribute(CLIENT_NAME_ATTRIBUTE)
                        .map(name -> KeyValue.of(ClientHttpObservationDocumentation.LowCardinalityKeyNames.CLIENT_NAME, (String) name))
                        .orElse(this.clientName(context));
    }

    private KeyValue uriKeyValue(ClientRequestObservationContext context) {
        return context.getRequest() == null ? KeyValue.of(URI, this.httpUrl(context).getValue()) :
                context.getRequest()
                        .attribute(URI_ATTRIBUTE)
                        .map(name -> KeyValue.of(URI, (String) name))
                        .orElse(KeyValue.of(URI, this.httpUrl(context).getValue()));
    }
}
