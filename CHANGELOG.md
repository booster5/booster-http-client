# Change Log

## v3.2.0
1. Update to Spring Boot 3.4.2
2. Update to Spring Cloud 2024.0.0
3. Update to Spring Cloud GCP 6.0.0
4. Update to JVM 21
5. Update dependencies

## v3.1.0
1. Update to Spring Boot 3.3.7
2. Update to Spring Cloud 2023.0.4
3. Update to Spring Cloud GCP 5.9.0

## v3.0.0
1. Update to Spring Boot 3.2.5

## v2.0.0
1. Move to different base.

## v1.5.0
1. Use common parent pom.

## v1.0.2
1. Update to booster-commons 1.0.1

## v1.0.1
1. Add more code coverage tests.
2. Handle response errors properly.

## v1.0.0
1. First version.
2. Requires Spring Boot 2.7.6 and 
3. Spring Cloud 2021.0.5
