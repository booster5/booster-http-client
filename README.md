# Booster Http Client

## Purpose

Booster HTTP client is based on Spring Boot ```WebClient``` to make it easier to 
build client calls. 

## Features 

1. Create ```WebClient``` from ```HttpClientConnectionSetting```, which allows one to:
   1. Create connection pools.
   2. Setup connection, read, write, and SSL handshake timeouts.
   3. Enable response compression automatically.
   4. Adjust memory used by ```WebClient```.
   5. Allow following redirects automatically.
2. Distributed tracing automatically enabled.
3. Parameterized path variables and query parameters automatically applied.
4. Record metrics, including response times, and failure, success counts easily.

### Why Retry and Circuit Breaker not Supported?

Retries and circuit breakers are supported in a more generic library: [booster-task](https://gitlab.com/booster5/booster-task).

Supporting on task level allows one not only have retries and circuit breakers on HTTP client
calls, but also on other tasks that are time consuming and can fail, such as invoking Redis, or 
call to a database.

Retry and circuit breaker enablement is provided in [booster-web-starter](https://gitlab.com/booster5/booster-web-starter), by allowing one 
to create a Task out of HTTP client, then apply optional retries and circuit breakers to 
that task.

### Why Not Run HtpClient in Separate Thread Pool?

Thread pools enablement is provided in booster-task as well, allowing one to decide when to 
run certain tasks in the calling thread, and when to run in dedicated thread pools. [Booster 
task](https://gitlab.com/booster5/booster-task) provides a generic framework for executing potentially concurrent tasks either in 
sequence or in parallel to each other, hence the option to use separate thread pools is 
provided in [booster-task](https://gitlab.com/booster5/booster-task).

### Metrics Reported 

Booster HTTP Client reports the following metrics for tasks:

| Metric Name           | Type    | Tag    | Tag Values                                        | Description                  |
|-----------------------|---------|--------|---------------------------------------------------|------------------------------|
| client_response_time  | timer   | name   |                                                   | client name                  |
| client_response_count | counter | name   |                                                   | client name                  |
|                       |         | status | fail, success                                     | execution status             |
|                       |         | reason | success, or exception simple name, or status code | reason for execution status  |

In addition to these metrics, custom metrics are also enabled by default, and trace IDs 
passed from client side to server.

## Getting started

### Project Setup

#### Maven

pom.xml
```xml
<dependency>
  <groupId>io.gitlab.booster</groupId>
  <artifactId>booster-http-client</artifactId>
  <version>VERSION_TO_USE</version>
</dependency>
```
if you haven't yet set up your repository:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/61046646/-/packages/maven</url>
  </repository>
</repositories>
```

#### Gradle Groovy

```groovy
implementation 'io.gitlab.booster:booster-http-client:VERSION_TO_USE'
```
setup maven repository:
```groovy
maven {
  url 'https://gitlab.com/api/v4/groups/61046646/-/packages/maven'
}
```

#### Gradle Kotlin

```kotlin
implementation("io.gitlab.booster:booster-http-client:VERSION_TO_USE")
```
setup maven repository:
```kotlin
maven("https://gitlab.com/api/v4/groups/61046646/-/packages/maven")
```
